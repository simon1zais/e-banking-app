<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Importieren des Stylsheets von Bootstrap -->
    <link rel="stylesheet" href="css/index-css.css">

    <!-- Titel des Taps wird Gesetzt -->
    <title>Register</title>
</head>
<body>

<!-- In diesem Div wird die gesammte webseite Programmiert-->
<div class="container">

    <!-- Der Titel der Seite wird Gesetzt -->
    <h1 class="mt-5 mb-5">Register:</h1>

    <?php

    ?>

    <form id="form_login" action="index-login.php" method="post">

        <!-- Hier wird eine Reihe erzeugt in dem das Suchinterface aufgelistet wird. -->

        <div class = "row">
            <div class="col-sm-6 form-group mb-3">

                <label for="email" class = "form-label">E-Mail:</label>
                <input id="email"
                       type="email"
                       name="email"
                       class="form-control"
                       required="required"
                />
            </div>
        </div>

        <div class = "row">
            <div class="col-sm-6 form-group mb-3">

                <label for="name" class = "form-label">User-Name:</label>
                <input id="name"
                       type="text"
                       name="name"
                       class="form-control"
                       required="required"
                />
            </div>
        </div>

        <div class = "row">
            <div class="col-sm-6 form-group mb-3">

                <label for="password" class = "form-label">Password:</label>
                <input id="password"
                       type="password"
                       name="password"
                       class="form-control"
                       required="required"
                />
            </div>
        </div>

        <div class = "row">
            <div class="col-sm-6 form-group mb-3">

                <label for="reenteredpassword" class = "form-label">Reenter Password:</label>
                <input id="reenteredpassword"
                       type="password"
                       name="reenteredpassword"
                       class="form-control"
                       required="required"
                />
            </div>
        </div>

        <div class = "row">

            <!-- Leitet den User zur Registrierung wieter -->
            <div class="col-sm-3 d-grid gap-2 mb-4">
                <a href="index-login.php" class="btn btn-secondary">Back</a>

                <!-- Leitet den zu seinen Account weiter-->
            </div>
            <div class="col-sm-3 d-grid gap-2 mb-4">
                <input type="submit"
                       name="submit"
                       class="btn btn-primary"
                       value="Register"
                />
            </div>
        </div>
    </form>
</body>
</html>
